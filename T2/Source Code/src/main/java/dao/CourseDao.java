package dao;

import model.Course;
import model.Student;

import java.util.List;

/**
 * Created by GligaBogdan on 15-Mar-16.
 */
public interface CourseDao {

    void createCourseTable();

    void deleteCourseTable();

    int insert(Course course);

    int delete(Course course);

    int update(Course course, int id);

    int enrollStudent(Student student, Course course);

    List<Student> getAllEnrolledStudents(Course course);

    Course selectById(int id);
}
