package dao;

import conf.ConnectionConfiguration;
import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GligaBogdan on 12-Mar-16.
 */
public class StudentDaoImpl implements StudentDao {

    public void createStudentTable() {


        Connection connection = ConnectionConfiguration.getConnection();

        try {
            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS student (id INT PRIMARY KEY UNIQUE, " +
                    "name VARCHAR(55), address VARCHAR(55), birth_date LONG)");

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void deleteStudentTable() {


        Connection connection = ConnectionConfiguration.getConnection();

        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE student");

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int insert(Student student) {


        int returnStatus = -1;

        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO student (id, name, address, birth_date)" +
                    "VALUES (?,?,?,?)");

            preparedStatement.setInt(1, student.getId());
            preparedStatement.setString(2, student.getName());
            preparedStatement.setString(3, student.getAddress());
            preparedStatement.setLong(4, student.getBirthDay());

            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;

    }

    public Student selectById(int id) {

        Student student = null;
        Connection connection = ConnectionConfiguration.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM student WHERE id = ? ");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();

            int receivedId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String address = resultSet.getString("address");
            long birthDay = resultSet.getLong("birth_date");

            student = new Student(receivedId,name, address, birthDay);

            resultSet.close();
            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


         return student;
    }

    public int delete(Student student) {


        int returnStatus = -1;

        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM student WHERE id = ?");

            preparedStatement.setInt(1, student.getId());
            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;
    }

    public int update(Student student, int id) {


        int returnStatus = -1;

        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE student SET " +
                    "name = ?, address = ?, birth_date = ? WHERE id = ?");

            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getAddress());
            preparedStatement.setLong(3, student.getBirthDay());
            preparedStatement.setInt(4, id);

            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;

    }
}
