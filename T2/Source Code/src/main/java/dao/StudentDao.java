package dao;

import model.Student;

import java.util.List;

/**
 * Created by GligaBogdan on 12-Mar-16.
 */
public interface StudentDao {

    void createStudentTable();

    void deleteStudentTable();

    int insert(Student student);

    int delete(Student student);

    int update(Student student, int id);

    Student selectById(int id);
}
