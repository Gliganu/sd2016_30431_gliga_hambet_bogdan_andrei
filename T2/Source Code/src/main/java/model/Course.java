package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GligaBogdan on 15-Mar-16.
 */
public class Course {

    private static int counter;
    private int id;
    private String name;
    private String teacher;
    private int studyYear;

    private List<Student> studentsList;

    public Course(String name, String teacher, int studyYear) {

        this(counter, name,teacher,studyYear);
        counter++;

    }

    public Course(int id,String name, String teacher, int studyYear) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
        this.studyYear = studyYear;
        this.studentsList = new ArrayList<Student>();
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public String getName() {
        return name;
    }

    public String getTeacher() {
        return teacher;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        if (studyYear != course.studyYear) return false;
        if (name != null ? !name.equals(course.name) : course.name != null) return false;
        if (teacher != null ? !teacher.equals(course.teacher) : course.teacher != null) return false;
        return studentsList != null ? studentsList.equals(course.studentsList) : course.studentsList == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (teacher != null ? teacher.hashCode() : 0);
        result = 31 * result + studyYear;
        result = 31 * result + (studentsList != null ? studentsList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teacher='" + teacher + '\'' +
                ", studyYear=" + studyYear +
                ", studentsList=" + studentsList +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }
}
